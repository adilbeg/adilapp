package si.tridens.adilapp;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@WebAppConfiguration
@ContextConfiguration(locations = "classpath:cucumber.xml")
@Cucumber.Options(format = {"pretty","json:target/json/output.json",  "html:target/html/"},
		features = "src/test/resource")



public class RunTest {

}
