package si.tridens.adilapp;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AdilappApplication.class, loader = SpringApplicationContextLoader.class)
@WebAppConfiguration
public class Def {
	private RestTemplate restTemplate;
	private String BaseURL = "http://localhost:8080/";
	private ResponseEntity<String> response;

	@Given("^the web context is set$")
	public void the_web_context_is_set() throws Throwable {
		restTemplate = new RestTemplate();
	}

	@When("^client request \"(.*?)\" \"(.*?)\"$")
	public void client_request(String arg1, String arg2) throws Throwable {
		switch (arg1) {
		case "GET":
			response = restTemplate.getForEntity(BaseURL + arg2, String.class);

			break;
		case "DELETE":
			response = restTemplate.exchange(BaseURL + arg2, HttpMethod.DELETE, new HttpEntity<String>(""),
					String.class);
			break;
		case "POST":
			response = restTemplate.exchange(BaseURL + arg2, HttpMethod.POST, new HttpEntity<String>(""), String.class);
			break;

		case "PUT":
			response = restTemplate.exchange(BaseURL + arg2, HttpMethod.PUT, new HttpEntity<String>(""), String.class);
			break;
		default:
			break;
		}
	}

	@Then("^the response code should be (\\d+)$")
	public void the_response_code_should_be(int arg1) throws Throwable {
		HttpStatus status = response.getStatusCode();
		String parseCode = Integer.toString(arg1);
		Assert.assertEquals(status.toString(), parseCode);
	}
	
	@Then("^the response code should not be (\\d+)$")
	public void the_response_code_should_not_be(int arg1) throws Throwable {
		HttpStatus status = response.getStatusCode();
		String parseCode = Integer.toString(arg1);
		Assert.assertNotEquals(status.toString(), parseCode);
	}

	@Then("^the JSON result should be$")
	public void the_JSON_result_should_be(String arg1) throws Throwable {
		System.out.print("Test: " + arg1 + " response" + response.getBody());
		Assert.assertEquals(response.getBody(), arg1);
	}

}
