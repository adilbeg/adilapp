package si.tridens.adilapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Ocena {

	@Id
	@GeneratedValue
	private long id;
	
	private long studentId;
	
	@Column(name="predmet_id")
	private long predmetId;
	private int ocena;

            
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="predmet_id", referencedColumnName="id",  insertable = false, updatable = false)
	private Predmet predmet;
	
	public int getOcena() {
		return ocena;
	}
	public void setOcena(int ocena) {
		this.ocena = ocena;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getStudentId() {
		return studentId;
	}
	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}
	public long getPredmetId() {
		return predmetId;
	}
	public void setPredmetId(long predmetId) {
		this.predmetId = predmetId;
	}
	public Predmet getPredmet() {
		return predmet;
	}
	public void setPredmet(Predmet predmet) {
		this.predmet = predmet;
	}
	
}
