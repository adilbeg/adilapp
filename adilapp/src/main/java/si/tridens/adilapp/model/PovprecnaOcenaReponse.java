package si.tridens.adilapp.model;

public class PovprecnaOcenaReponse {

	private Student student;
	private double povprecje;

	public double getPovprecje() {
		return povprecje;
	}

	public void setPovprecje(double povprecje) {
		this.povprecje = povprecje;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
	
	
	
}
