package si.tridens.adilapp.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import si.tridens.adilapp.model.Predmet;



public interface PredmetRepository extends CrudRepository<Predmet, Long>{
	List<Predmet> findByNaziv(String naziv);
	Predmet findById(long id);
}
