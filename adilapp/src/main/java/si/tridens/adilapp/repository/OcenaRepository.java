package si.tridens.adilapp.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import si.tridens.adilapp.model.Ocena;

public interface OcenaRepository extends CrudRepository<Ocena, Long>{

	Ocena findById(long id);
	List<Ocena> findByStudentIdAndPredmetId(long studentId, long predmetId);
	List<Ocena> findByStudentId(long studentId);
	List<Ocena> findByPredmetId(long predmetId);
}
