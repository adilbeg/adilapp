package si.tridens.adilapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdilappApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdilappApplication.class, args);
	}
}
