package si.tridens.adilapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import si.tridens.adilapp.model.Predmet;
import si.tridens.adilapp.model.Student;
import si.tridens.adilapp.repository.PredmetRepository;


@Controller
@RequestMapping("/api/predmet")
public class PredmetController {
	
	@Autowired
	private PredmetRepository predmetRepository;
	
	@RequestMapping(value="", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Predmet> createPredmet(@RequestBody  Predmet predmet)
	{
		predmetRepository.save(predmet);
		return new ResponseEntity<Predmet>(predmet, HttpStatus.CREATED);
	}
	
	//update subject
		@RequestMapping(value="/update", method = RequestMethod.PUT)
		@ResponseBody
		public ResponseEntity<Predmet> updatePredmet(@RequestBody  Predmet predmet)
		{
			predmetRepository.save(predmet);
			return new ResponseEntity<Predmet>(predmet, HttpStatus.OK);
		}
	
	@RequestMapping(value="/{id}")
	@ResponseBody
	public ResponseEntity<Predmet> getPredmet(@PathVariable("id") long id)
	{
		Predmet predmet = predmetRepository.findById(id);
		return new ResponseEntity<Predmet>(predmet, HttpStatus.OK);
	}
	
	@RequestMapping("/all")
	@ResponseBody
	public ResponseEntity<List<Predmet>> getPredmeti()
	{
		List<Predmet> predmeti = (List<Predmet>) predmetRepository.findAll();
		return new ResponseEntity<List<Predmet>>(predmeti, HttpStatus.OK);
	}

	@RequestMapping(value="/remove/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseEntity<Long> deletePredmet(@PathVariable("id") long id){
		predmetRepository.delete(id);
		return new ResponseEntity<>(id, HttpStatus.OK);
	}

}
