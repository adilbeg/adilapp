package si.tridens.adilapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import si.tridens.adilapp.model.Ocena;
import si.tridens.adilapp.model.PovprecnaOcenaReponse;
import si.tridens.adilapp.model.Predmet;
import si.tridens.adilapp.model.Student;
import si.tridens.adilapp.repository.OcenaRepository;
import si.tridens.adilapp.repository.StudentRepository;

@Controller
@RequestMapping("/api/ocena")
public class OcenaController {

	@Autowired
	private OcenaRepository ocenaRepository;
	@Autowired
	private StudentRepository studentRepository;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Ocena> createOcena(@RequestBody Ocena ocena) {
		ocenaRepository.save(ocena);
		ocena = ocenaRepository.findById(ocena.getId());
		return new ResponseEntity<Ocena>(ocena, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}")
	@ResponseBody
	public ResponseEntity<Ocena> getOcena(@PathVariable("id") long id) {
		Ocena ocena = ocenaRepository.findById(id);
		return new ResponseEntity<Ocena>(ocena, HttpStatus.OK);
	}

	@RequestMapping("/all")
	@ResponseBody
	public ResponseEntity<List<Ocena>> getOcene() {
		List<Ocena> ocene = (List<Ocena>) ocenaRepository.findAll();
		return new ResponseEntity<List<Ocena>>(ocene, HttpStatus.OK);
	}

	@RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseEntity<Long> deleteOcena(@PathVariable("id") long id) {
		ocenaRepository.delete(id);
		return new ResponseEntity<>(id, HttpStatus.OK);
	}

	@RequestMapping(value = "/student/{studentId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<Ocena>> getStudentsOcene(@PathVariable("studentId") long studentId) {
		List<Ocena> ocene = (List<Ocena>) ocenaRepository.findByStudentId(studentId);
		return new ResponseEntity<List<Ocena>>(ocene, HttpStatus.OK);
	}

	@RequestMapping(value = "/povprecje/{studentId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<PovprecnaOcenaReponse> getStudentsPovprecje(@PathVariable("studentId") long studentId) {

		PovprecnaOcenaReponse povprecnaOcenaReponse = new PovprecnaOcenaReponse();

		Student student = studentRepository.findById(studentId);
		if (student != null) {
			povprecnaOcenaReponse.setStudent(student);

			List<Ocena> ocene = (List<Ocena>) ocenaRepository.findByStudentId(studentId);

			if (ocene.size() > 0) {
				double sum = 0;

				for (Ocena ocena : ocene) {
					sum = sum + ocena.getOcena();
				}

				povprecnaOcenaReponse.setPovprecje(sum / ocene.size());
			}
		}

		return new ResponseEntity<PovprecnaOcenaReponse>(povprecnaOcenaReponse, HttpStatus.OK);
	}

}
