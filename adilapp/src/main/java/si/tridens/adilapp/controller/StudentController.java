package si.tridens.adilapp.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import si.tridens.adilapp.model.Student;
import si.tridens.adilapp.repository.StudentRepository;


@Controller
@RequestMapping("/api/student")
public class StudentController {
	
	@Autowired
	private StudentRepository studentRepository;
	

	@RequestMapping(value="", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Student> createStudent(@RequestBody  Student student)
	{
		studentRepository.save(student);
		return new ResponseEntity<Student>(student, HttpStatus.CREATED);
	}
	
	//update student
	@RequestMapping(value="/update", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<Student> updateStudent(@RequestBody  Student student)
	{
		studentRepository.save(student);
		if(student == null){
			return new ResponseEntity<Student>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Student>(student, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}")
	@ResponseBody
	public ResponseEntity<Student> getStudent(@PathVariable("id") long id)
	{
		Student student = studentRepository.findById(id);
		if(student == null){
			return new ResponseEntity<Student>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Student>(student, HttpStatus.OK);
	}
	
	@RequestMapping("/all")
	@ResponseBody
	public ResponseEntity<List<Student>> getStudents()
	{
		List<Student> students = (List<Student>) studentRepository.findAll();
		return new ResponseEntity<List<Student>>(students, HttpStatus.OK);
	}
	
	

	@RequestMapping(value="/remove/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseEntity<Long> deleteStudent(@PathVariable("id") long id){
		studentRepository.delete(id);
		return new ResponseEntity<>(id, HttpStatus.NO_CONTENT);
	}

}
