$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Student.feature");
formatter.feature({
  "line": 1,
  "name": "Find all student",
  "description": "",
  "id": "find-all-student",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 3,
  "name": "find by id",
  "description": "",
  "id": "find-all-student;find-by-id",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "the web context is set",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "client request GET /api/student/al",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "the response code should be 300",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.the_web_context_is_set()"
});
formatter.result({
  "duration": 170480116,
  "status": "passed"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({
  "arguments": [
    {
      "val": "300",
      "offset": 28
    }
  ],
  "location": "Steps.the_response_code_should_be(int)"
});
formatter.result({
  "status": "skipped"
});
});